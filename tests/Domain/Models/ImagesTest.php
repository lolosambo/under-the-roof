<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Models;

use App\Domain\Models\Galleries;
use App\Domain\Models\Images;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ImagesTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class ImagesTest extends TestCase
{

    private $image;

    public function setUp()
    {
        $this->image = new Images('images/uploads/testImage.jpg');
        $gallery = new Galleries('test Gallery');
        $this->image->setGallery($gallery);
    }

    /**
     * @group unit
     */
    public function testImagesConstruct()
    {
        static::assertInstanceof(Images::class, $this->image);
    }

    /**
     * @group unit
     */
    public function testImagesAttributes()
    {
        static::assertObjectHasAttribute('id', $this->image);
        static::assertObjectHasAttribute('gallery', $this->image);
        static::assertObjectHasAttribute('aLaUne', $this->image);
        static::assertObjectHasAttribute('url', $this->image);
    }

    /**
     * @group unit
     */
    public function testImagesMustHaveValidAttributes()
    {
        $image= $this->createMock(Images::class);
        static::assertInstanceOf(UuidInterface::class, $image->getId());
        static::assertInstanceOf(Galleries::class, $this->image->getGallery());
        static::assertInternalType('int', $this->image->getALaUne());
        static::assertInternalType('string', $this->image->getUrl());

    }
}
