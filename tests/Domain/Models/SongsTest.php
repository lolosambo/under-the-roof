<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Models;

use App\Domain\Models\Albums;
use App\Domain\Models\Songs;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class SongsTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class SongsTest extends TestCase
{

    private $song;

    public function setUp()
    {
        $this->song = new Songs("Song Test Title");
        $album = new Albums('Album Test', 2018);
        $this->song->setAlbum($album);
        $this->song->setTime(new \DateTime('4:20'));
        $this->song->setUrlAudio('https://www.undertheroof.fr/audio/audioTest.mp3');
        $this->song->setUrlVideo('https://www.youtu.be/GETXGTSXGDF');
    }

    /**
     * @group unit
     */
    public function testSongsConstruct()
    {
        static::assertInstanceof(Songs::class, $this->song);
    }

    /**
     * @group unit
     */
    public function testSongsAttributes()
    {
        static::assertObjectHasAttribute('id', $this->song);
        static::assertObjectHasAttribute('title', $this->song);
        static::assertObjectHasAttribute('author', $this->song);
        static::assertObjectHasAttribute('time', $this->song);
        static::assertObjectHasAttribute('urlAudio', $this->song);
        static::assertObjectHasAttribute('urlVideo', $this->song);
        static::assertObjectHasAttribute('album', $this->song);
    }

    /**
     * @group unit
     */
    public function testSongsMustHaveValidAttributes()
    {
        $song= $this->createMock(Songs::class);
        static::assertInstanceOf(UuidInterface::class, $song->getId());
        static::assertInternalType('string', $this->song->getTitle());
        static::assertInternalType('string', $this->song->getAuthor());
        static::assertInstanceOf(\DateTime::class, $this->song->getTime());
        static::assertInternalType('string', $this->song->getUrlAudio());
        static::assertInternalType('string', $this->song->geturlVideo());
        static::assertInstanceOf(Albums::class, $this->song->getAlbum());
    }
}


