<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Models;

use App\Domain\Models\Articles;
use App\Domain\Models\Comments;
use App\Domain\Models\Users;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class CommentsTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CommentsTest extends TestCase
{

    private $comment;

    public function setUp()
    {
        $this->comment = new Comments("Some content for a Comment exemple");
        $user = new Users(
            "TestUser",
            "MySuperPassword",
            "testuser@testprovider.com"
        );
        $article = new Articles('Article Test', 'Some content for the test article');
        $this->comment->setUser($user);
        $this->comment->setArticle($article);
        $this->comment->setCreatedAt();
    }

    /**
     * @group unit
     */
    public function testCommentsConstruct()
    {
        static::assertInstanceof(Comments::class, $this->comment);
    }

    /**
     * @group unit
     */
    public function testCommentsAttributes()
    {
        static::assertObjectHasAttribute('id', $this->comment);
        static::assertObjectHasAttribute('content', $this->comment);
        static::assertObjectHasAttribute('createdAt', $this->comment);

    }

    /**
     * @group unit
     */
    public function testCommentsMustHaveValidAttributes()
    {
        $comment= $this->createMock(Comments::class);
        static::assertInstanceOf(UuidInterface::class, $comment->getId());
        static::assertInternalType('string', $this->comment->getContent());
        static::assertInstanceOf(\DateTime::class, $this->comment->getCreatedAt());
        static::assertInstanceOf(Users::class, $this->comment->getUser());
        static::assertInstanceOf(Articles::class, $this->comment->getArticle());
    }
}


