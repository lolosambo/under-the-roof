<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Models;

use App\Domain\Models\Avatars;
use App\Domain\Models\Comments;
use App\Domain\Models\Users;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class UsersTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class UsersTest extends TestCase
{

    private $user;

    public function setUp()
    {
        $user = new Users(
            "TestUser",
            "MySuperPassword",
            "testuser@testprovider.com"
        );
        $user->setCreatedAt();
        $this->user = $user;
    }

    /**
     * @group unit
     */
    public function testUsersConstruct()
    {
        static::assertInstanceof(Users::class, $this->user);
    }

    /**
     * @group unit
     */
    public function testUsersAttributes()
    {
        static::assertObjectHasAttribute('id', $this->user);
        static::assertObjectHasAttribute('userName', $this->user);
        static::assertObjectHasAttribute('password', $this->user);
        static::assertObjectHasAttribute('mail', $this->user);
        static::assertObjectHasAttribute('verified', $this->user);
        static::assertObjectHasAttribute('createdAt', $this->user);
        static::assertObjectHasAttribute('comments', $this->user);
        static::assertObjectHasAttribute('avatars', $this->user);
    }

    /**
     * @group unit
     */
    public function testUsersMustHaveValidAttributes()
    {
        $user= $this->createMock(Users::class);
        static::assertInstanceOf(UuidInterface::class, $user->getId());
        static::assertInternalType('string', $this->user->getUsername());
        static::assertInternalType('string', $this->user->getSalt());
        static::assertInternalType('string', $this->user->getMail());
        static::assertInternalType('int', $this->user->getVerified());
        static::assertInstanceOf(\DateTime::class, $this->user->getCreatedAt());
        static::assertInstanceOf(ArrayCollection::class, $this->user->getComments());
        static::assertInstanceOf(ArrayCollection::class, $this->user->getAvatars());
    }

    /**
     * @group unit
     */
    public function testAddFunctions()
    {
        $comment = $this->createMock(Comments::class);
        $avatar = $this->createMock(Avatars::class);
        $this->user->addComment($comment);
        $this->user->addAvatar($avatar);
        static::assertInstanceOf(Comments::class, $this->user->getComments()->first());
        static::assertInstanceOf(Avatars::class, $this->user->getAvatars()->first());

    }

    /**
     * @group unit
     */
    public function testRemoveFunctions()
    {
        $comment = $this->createMock(Comments::class);
        $avatar = $this->createMock(Avatars::class);
        $this->user->addComment($comment);
        $this->user->addAvatar($avatar);
        $this->user->removeComment($comment);
        $this->user->removeAvatar($avatar);
        static::assertNull(null, $this->user->getComments());
        static::assertNull(null, $this->user->getAvatars());

    }

    /**
     * @group unit
     */
    public function testPasswordMustBeCrypted()
    {
        static::assertContains('MySuperPassword', $this->user->getSalt());
    }

    /**
     * @group unit
     */
    public function testEmailAddressMustMatchWithPattern()
    {
        static::assertTrue($this->user->setMail('test@gmail.com'));
    }

    /**
     * @group unit
     */
    public function testEmailAddressMustReturnNullWithBadPattern()
    {
        static::assertFalse($this->user->setMail('tes/t@gmail.com51'));
    }

    /**
     * @group unit
     */
    public function testVerifiedAttributeMustBe0Or1()
    {
        static::assertSame(0, $this->user->getVerified());
        $this->user->setVerified(1);
        static::assertSame(1, $this->user->getVerified());

    }

    /**
     * @group unit
     */
    public function testVerifiedAttributeMustReturnFalseWithBadValues()
    {
        $this->user->setVerified(3);
        static::assertSame(0, $this->user->getVerified());
    }

}




