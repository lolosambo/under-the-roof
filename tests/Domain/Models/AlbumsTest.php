<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Models;

use App\Domain\Models\Albums;
use App\Domain\Models\Songs;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AlbumsTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class AlbumsTest extends TestCase
{

    private $album;

    public function setUp()
    {
        $this->album = new Albums(
            "album Test",
            2018
        );
        $this->album->setCoverUrl('https://www.youtu.be/GETXGTSXGDF');
    }

    /**
     * @group unit
     */
    public function testAlbumsConstruct()
    {
        static::assertInstanceof(Albums::class, $this->album);
    }

    /**
     * @group unit
     */
    public function testAlbumsAttributes()
    {
        static::assertObjectHasAttribute('id', $this->album);
        static::assertObjectHasAttribute('title', $this->album);
        static::assertObjectHasAttribute('author', $this->album);
        static::assertObjectHasAttribute('songs', $this->album);
        static::assertObjectHasAttribute('year', $this->album);
        static::assertObjectHasAttribute('coverUrl', $this->album);
    }

    /**
     * @group unit
     */
    public function testAlbumsMustHaveValidAttributes()
    {
        $album = $this->createMock(Albums::class);
        static::assertInstanceOf(UuidInterface::class, $album->getId());
        static::assertInternalType('string', $this->album->getTitle());
        static::assertInternalType('string', $this->album->getAuthor());
        static::assertContains('Under The Roof', $this->album->getAuthor());
        static::assertInstanceOf(ArrayCollection::class, $this->album->getSongs());
        static::assertInternalType('int', $this->album->getYear());
        static::assertInternalType('string', $this->album->getCoverUrl());
    }

    /**
     * @group unit
     */
    public function testAddFunctions()
    {
        $song = $this->createMock(Songs::class);
        $this->album->addSong($song);
        static::assertInstanceOf(Songs::class, $this->album->getSongs()->first());
    }

    /**
     * @group unit
     */
    public function testRemoveFunctions()
    {
        $song = $this->createMock(Songs::class);
        $this->album->addSong($song);
        $this->album->removeSong($song);
        static::assertNull(null, $this->album->getSongs());
    }
}