<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Models;

use App\Domain\Models\Albums;
use App\Domain\Models\Avatars;
use App\Domain\Models\Users;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AvatarsTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class AvatarsTest extends TestCase
{

    private $avatar;

    public function setUp()
    {
        $this->avatar = new Avatars("http://www.undertheroof.fr/images/avatars/exemple.png");
        $user = new Users('UserTest', 'MySuperPassword', "usertest@undertheroof.fr");
        $this->avatar->setUser($user);
    }

    /**
     * @group unit
     */
    public function testAvatarsConstruct()
    {
        static::assertInstanceof(Avatars::class, $this->avatar);
    }

    /**
     * @group unit
     */
    public function testAvatarsAttributes()
    {
        static::assertObjectHasAttribute('id', $this->avatar);
        static::assertObjectHasAttribute('url', $this->avatar);
        static::assertObjectHasAttribute('user', $this->avatar);
    }

    /**
     * @group unit
     */
    public function testAvatarsMustHaveValidAttributes()
    {
        $avatar= $this->createMock(Avatars::class);
        static::assertInstanceOf(UuidInterface::class, $avatar->getId());
        static::assertInternalType('string', $this->avatar->getUrl());
        static::assertInstanceOf(Users::class, $this->avatar->getUser());
    }
}


