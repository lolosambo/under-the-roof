<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Models;

use App\Domain\Models\Articles;
use App\Domain\Models\Galleries;
use App\Domain\Models\Images;
use App\Domain\Models\Interfaces\ArticlesInterface;
use App\Domain\Models\Songs;
use App\Domain\Models\Videos;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class GalleriesTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class GalleriesTest extends TestCase
{

    private $gallery;

    private $image;

    private $video;

    public function setUp()
    {
        $this->gallery = new Galleries('Test Gallery Name');
        $article = new Articles('Article Test', 'Some content for the test article');
        $this->gallery->setArticle($article);

        $this->image = $this->createMock(Images::class);
        $this->gallery->addImage($this->image);
        $this->video = $this->createMock(Videos::class);
        $this->gallery->addVideo($this->video);
    }

    /**
     * @group unit
     */
    public function testGalleriesConstruct()
    {
        static::assertInstanceof(Galleries::class, $this->gallery);
    }

    /**
     * @group unit
     */
    public function testGalleriesAttributes()
    {
        static::assertObjectHasAttribute('id', $this->gallery);
        static::assertObjectHasAttribute('name', $this->gallery);
        static::assertObjectHasAttribute('images', $this->gallery);
        static::assertObjectHasAttribute('videos', $this->gallery);
        static::assertObjectHasAttribute('article', $this->gallery);
    }

    /**
     * @group unit
     */
    public function testGalleriesMustHaveValidAttributes()
    {
        $gallery = $this->createMock(Galleries::class);
        static::assertInstanceOf(UuidInterface::class, $gallery->getId());
        static::assertInternalType('string', $this->gallery->getName());
        static::assertInstanceOf(ArrayCollection::class, $this->gallery->getImages());
        static::assertInstanceOf(ArrayCollection::class, $this->gallery->getVideos());
        static::assertInstanceOf(ArticlesInterface::class, $this->gallery->getArticle());

    }

    /**
     * @group unit
     */
    public function testAddFunctions()
    {
        static::assertInstanceOf(Images::class, $this->gallery->getImages()->first());
        static::assertInstanceOf(Videos::class, $this->gallery->getVideos()->first());
    }

    /**
     * @group unit
     */
    public function testRemoveFunctions()
    {
        $this->gallery->removeImage($this->image);
        static::assertNull(null, $this->gallery->getImages());
        $this->gallery->removeVideo($this->video);
        static::assertNull(null, $this->gallery->getVideos());
    }
}