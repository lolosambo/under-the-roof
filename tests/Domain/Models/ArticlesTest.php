<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Models;

use App\Domain\Models\Articles;
use App\Domain\Models\Categories;
use App\Domain\Models\Comments;
use App\Domain\Models\Users;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ArticlesTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class ArticlesTest extends TestCase
{

    private $article;

    public function setUp()
    {
        $this->article = new Articles(
            "Article Test",
            "Some content for the Article test"
        );
        $this->article->setCreatedAt();
        $this->article->setUser($this->createMock(Users::class));
        $this->article->setCategory($this->createMock(Categories::class));
        $this->article->convertSlug($this->article->getTitle());

    }

    /**
     * @group unit
     */
    public function testUsersConstruct()
    {
        static::assertInstanceof(Articles::class, $this->article);
    }

    /**
     * @group unit
     */
    public function testArticlesAttributes()
    {
        static::assertObjectHasAttribute('id', $this->article);
        static::assertObjectHasAttribute('title', $this->article);
        static::assertObjectHasAttribute('slug', $this->article);
        static::assertObjectHasAttribute('content', $this->article);
        static::assertObjectHasAttribute('createdAt', $this->article);
        static::assertObjectHasAttribute('user', $this->article);
        static::assertObjectHasAttribute('comments', $this->article);
        static::assertObjectHasAttribute('category', $this->article);
    }

    /**
     * @group unit
     */
    public function testArticlesMustHaveValidAttributes()
    {
        $article= $this->createMock(Articles::class);
        static::assertInstanceOf(UuidInterface::class, $article->getId());
        static::assertInternalType('string', $this->article->getTitle());
        static::assertInternalType('string', $this->article->getSlug());
        static::assertInternalType('string', $this->article->getContent());
        static::assertInstanceOf(\DateTime::class, $this->article->getCreatedAt());
        static::assertInstanceOf(Users::class, $this->article->getUser());
        static::assertInstanceOf(ArrayCollection::class, $this->article->getComments());
        static::assertInstanceOf(Categories::class, $this->article->getCategory());
    }

    /**
     * @group unit
     */
    public function testAddFunctions()
    {
        $comment = $this->createMock(Comments::class);
        $this->article->addComment($comment);
        static::assertInstanceOf(Comments::class, $this->article->getComments()->first());
    }

    /**
     * @group unit
     */
    public function testRemoveFunctions()
    {
        $comment = $this->createMock(Comments::class);
        $this->article->addComment($comment);
        $this->article->removeComment($comment);
        static::assertNull(null, $this->article->getComments());
    }

    /**
     * @group unit
     */
    public function testConvertSlugFunction()
    {
        static::assertContains('Article_Test', $this->article->getSlug());
    }
}



