<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Models;

use App\Domain\Models\Articles;
use App\Domain\Models\Categories;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class CategoriesTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CategoriesTest extends TestCase
{

    private $category;

    public function setUp()
    {
        $this->category = new Categories("CategoryTest");

    }

    /**
     * @group unit
     */
    public function testCategoriesConstruct()
    {
        static::assertInstanceof(Categories::class, $this->category);
    }

    /**
     * @group unit
     */
    public function testCategoriesAttributes()
    {
        static::assertObjectHasAttribute('id', $this->category);
        static::assertObjectHasAttribute('name', $this->category);
        static::assertObjectHasAttribute('articles', $this->category);
    }

    /**
     * @group unit
     */
    public function testCategoriesMustHaveValidAttributes()
    {
        $category= $this->createMock(Categories::class);
        $article = $this->createMock(Articles::class);
        $this->category->addArticle($article);
        static::assertInstanceOf(UuidInterface::class, $category->getId());
        static::assertInternalType('string', $this->category->getName());
        static::assertInstanceOf(ArrayCollection::class, $this->category->getArticles());
    }

    /**
     * @group unit
     */
    public function testAddFunctions()
    {
        $article = $this->createMock(Articles::class);
        $this->category->addArticle($article);
        static::assertInstanceOf(Articles::class, $this->category->getArticles()->first());
    }

    /**
     * @group unit
     */
    public function testRemoveFunctions()
    {
        $article = $this->createMock(Articles::class);
        $this->category->addArticle($article);
        $this->category->removeArticle($article);
        static::assertNull(null, $this->category->getArticles());
    }
}


