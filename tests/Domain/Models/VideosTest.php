<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Models;

use App\Domain\Models\Galleries;
use App\Domain\Models\Videos;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class VideosTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class VideosTest extends TestCase
{

    private $video;

    public function setUp()
    {
        $this->video = new Videos('https://www.youtu.be/GETXGTSXGDF');
        $gallery = new Galleries('Test Gallery');
        $this->video->setGallery($gallery);
    }

    /**
     * @group unit
     */
    public function testVideosConstruct()
    {
        static::assertInstanceof(Videos::class, $this->video);
    }

    /**
     * @group unit
     */
    public function testVideosAttributes()
    {
        static::assertObjectHasAttribute('id', $this->video);
        static::assertObjectHasAttribute('gallery', $this->video);
        static::assertObjectHasAttribute('url', $this->video);
    }

    /**
     * @group unit
     */
    public function testVideosMustHaveValidAttributes()
    {
        $video= $this->createMock(Videos::class);
        static::assertInstanceOf(UuidInterface::class, $video->getId());
        static::assertInstanceOf(Galleries::class, $this->video->getGallery());
        static::assertInternalType('string', $this->video->getUrl());

    }
}
