<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Forms\DTO;

use App\Domain\Forms\DTO\CreateCategoryDTO;
use PHPUnit\Framework\TestCase;

/**
 * Class CreateCategoryDTOTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CreateCategoryDTOTest extends TestCase
{

    private $dto;

    public function setUp()
    {
        $this->dto = new CreateCategoryDTO("test Category Name");
    }

    /**
     * @group unit
     */
    public function testCategoryDTOConstruct()
    {
        static::assertInstanceof(CreateCategoryDTO::class, $this->dto);
    }

    /**
     * @group unit
     */
    public function testCategoryDTOAttributes()
    {
        static::assertObjectHasAttribute('name', $this->dto);
    }

    /**
     * @group unit
     */
    public function testCategoryDTOMustHaveValidAttributes()
    {
        static::assertInternalType('string', $this->dto->name);
    }
}