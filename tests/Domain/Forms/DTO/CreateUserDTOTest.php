<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Forms\DTO;

use App\Domain\Forms\DTO;
use App\Domain\Forms\DTO\CreateUserDTO;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidInterface;

/**
 * Class CreateUserDTOTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CreateUserDTOTest extends TestCase
{

    private $dto;

    public function setUp()
    {
        $this->dto = new CreateUserDTO(
            "testUsername",
            "MySuperPassword",
            "testEmail@provider.com"
        );
    }

    /**
     * @group unit
     */
    public function testUsersDTOConstruct()
    {
        static::assertInstanceof(CreateUserDTO::class, $this->dto);
    }

    /**
     * @group unit
     */
    public function testUsersDTOAttributes()
    {
        static::assertObjectHasAttribute('username', $this->dto);
        static::assertObjectHasAttribute('password', $this->dto);
        static::assertObjectHasAttribute('mail', $this->dto);
    }

    /**
     * @group unit
     */
    public function testUsersDTOMustHaveValidAttributes()
    {
        static::assertInternalType('string', $this->dto->username);
        static::assertInternalType('string', $this->dto->password);
        static::assertInternalType('string', $this->dto->mail);
    }
}