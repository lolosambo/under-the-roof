<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Forms\DTO;

use App\Domain\Forms\DTO\CreateCommentDTO;
use PHPUnit\Framework\TestCase;

/**
 * Class CreateCommentDTOTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CreateCommentDTOTest extends TestCase
{

    private $dto;

    public function setUp()
    {
        $this->dto = new CreateCommentDTO("test Comment Content");
    }

    /**
     * @group unit
     */
    public function testCommentDTOConstruct()
    {
        static::assertInstanceof(CreateCommentDTO::class, $this->dto);
    }

    /**
     * @group unit
     */
    public function testCommentDTOAttributes()
    {
        static::assertObjectHasAttribute('content', $this->dto);
    }

    /**
     * @group unit
     */
    public function testCommentDTOMustHaveValidAttributes()
    {
        static::assertInternalType('string', $this->dto->content);
    }
}