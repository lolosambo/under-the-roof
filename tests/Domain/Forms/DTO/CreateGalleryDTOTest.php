<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Forms\DTO;

use App\Domain\Forms\DTO\CreateGalleryDTO;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

/**
 * Class CreateGalleryDTOTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CreateGalleryDTOTest extends TestCase
{

    private $dto;

    private $images;

    private $videos;

    public function setUp()
    {
        $this->images = $this->createMock(ArrayCollection::class);
        $this->videos = $this->createMock(ArrayCollection::class);
        $this->dto = new CreateGalleryDTO(
            "gallery Test",
            $this->images,
            $this->videos
        );
    }

    /**
     * @group unit
     */
    public function testGalleryDTOConstruct()
    {
        static::assertInstanceof(CreateGalleryDTO::class, $this->dto);
    }

    /**
     * @group unit
     */
    public function testGalleryDTOAttributes()
    {
        static::assertObjectHasAttribute('name', $this->dto);
        static::assertObjectHasAttribute('images', $this->dto);
        static::assertObjectHasAttribute('videos', $this->dto);
    }

    /**
     * @group unit
     */
    public function testGalleryDTOMustHaveValidAttributes()
    {
        static::assertInternalType('string', $this->dto->name);
        static::assertInstanceOf(ArrayCollection::class, $this->dto->images);
        static::assertInstanceOf(ArrayCollection::class, $this->dto->videos);

    }
}