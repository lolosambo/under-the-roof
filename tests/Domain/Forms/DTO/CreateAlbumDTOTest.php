<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Forms\DTO;

use App\Domain\Forms\DTO\CreateAlbumDTO;
use PHPUnit\Framework\TestCase;

/**
 * Class CreateArticleDTOTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CreateAlbumDTOTest extends TestCase
{

    private $dto;

    public function setUp()
    {
        $this->dto = new CreateAlbumDTO(
            "test Album Title",
            "Under The Roof",
            2008,
            'http://www.undertheroof.fr/images/covers/testCover.png'
        );
    }

    /**
     * @group unit
     */
    public function testAlbumsDTOConstruct()
    {
        static::assertInstanceof(CreateAlbumDTO::class, $this->dto);
    }

    /**
     * @group unit
     */
    public function testAlbumsDTOAttributes()
    {
        static::assertObjectHasAttribute('title', $this->dto);
        static::assertObjectHasAttribute('author', $this->dto);
        static::assertObjectHasAttribute('year', $this->dto);
        static::assertObjectHasAttribute('coverUrl', $this->dto);
    }

    /**
     * @group unit
     */
    public function testAlbumsDTOMustHaveValidAttributes()
    {
        static::assertInternalType('string', $this->dto->title);
        static::assertInternalType('string', $this->dto->author);
        static::assertInternalType('int', $this->dto->year);
        static::assertInternalType('string', $this->dto->coverUrl);

    }
}