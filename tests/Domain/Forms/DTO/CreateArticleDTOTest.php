<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Forms\DTO;

use App\Domain\Forms\DTO\CreateArticleDTO;
use App\Domain\Models\Categories;
use App\Domain\Models\Interfaces\CategoriesInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class CreateArticleDTOTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CreateArticleDTOTest extends TestCase
{

    private $dto;

    public function setUp()
    {
        $category = new Categories("Category Test");
        $this->dto = new CreateArticleDTO(
            "test Article Title",
            "My super article's content",
            $category

        );
    }

    /**
     * @group unit
     */
    public function testArticlesDTOConstruct()
    {
        static::assertInstanceof(CreateArticleDTO::class, $this->dto);
    }

    /**
     * @group unit
     */
    public function testArticlesDTOAttributes()
    {
        static::assertObjectHasAttribute('category', $this->dto);
        static::assertObjectHasAttribute('title', $this->dto);
        static::assertObjectHasAttribute('content', $this->dto);
    }

    /**
     * @group unit
     */
    public function testArticlesDTOMustHaveValidAttributes()
    {
        static::assertInstanceOf(CategoriesInterface::class, $this->dto->category);
        static::assertInternalType('string', $this->dto->title);
        static::assertInternalType('string', $this->dto->content);

    }
}