<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Forms\DTO;

use App\Domain\Forms\DTO\CreateAvatarDTO;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class CreateAvatarDTOTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CreateAvatarDTOTest extends TestCase
{

    private $dto;

    public function setUp()
    {
        $file = $this->createMock(UploadedFile::class);
        $this->dto = new CreateAvatarDTO($file);
    }

    /**
     * @group unit
     */
    public function testAvatarDTOConstruct()
    {
        static::assertInstanceof(CreateAvatarDTO::class, $this->dto);
    }

    /**
     * @group unit
     */
    public function testAvatarDTOAttributes()
    {
        static::assertObjectHasAttribute('avatar', $this->dto);
    }

    /**
     * @group unit
     */
    public function testAvatarDTOMustHaveValidAttributes()
    {
        static::assertInstanceOf(UploadedFile::class, $this->dto->avatar);
    }
}