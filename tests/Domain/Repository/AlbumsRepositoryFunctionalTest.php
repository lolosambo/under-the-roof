<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Domain\Repository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AlbumsRepositoryFunctionalTest
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class AlbumsRepositoryFunctionalTest extends WebTestCase
{


}