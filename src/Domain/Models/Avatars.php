<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models;


use App\Domain\Models\Interfaces\AvatarsInterface;
use App\Domain\Models\Interfaces\UsersInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Avatars
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class Avatars implements AvatarsInterface
{
    /**
     * @var UuidInterface $id
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var UsersInterface $user
     */
    private $user;


    /**
     * Avatars constructor.
     *
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return UsersInterface
     */
   public function getUser()
   {
       return $this->user;
   }

    /**
     * @param UsersInterface $user
     */
   public function setUser(UsersInterface $user)
   {
       $this->user = $user;
   }
}
