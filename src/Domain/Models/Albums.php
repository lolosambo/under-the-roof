<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models;

use App\Domain\Models\Interfaces\SongsInterface;
use App\Domain\Models\Interfaces\AlbumsInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Albums
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class Albums implements AlbumsInterface
{
    /**
     * @var UuidInterface $id
     */
    private $id;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var string $author
     */
    private $author = "Under The Roof";

    /**
     * @var ArrayCollection  $songs
     */
    private $songs;

    /**
     * @var int $year
     */
    private $year;

    /**
     * @var string $coverUrl
     */
    private $coverUrl;

    /**
     * Albums constructor.
     *
     * @param string $title
     * @param int $year
     */
    public function __construct(
        string $title,
        int $year
    ) {
        $this->title = $title;
        $this->year = $year;
        $this->songs = new ArrayCollection();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }

    /**
     * @return ArrayCollection
     */
    public function getSongs()
    {
        return $this->songs;
    }

    /**
     * @param SongsInterface $song
     */
    public function addSong(SongsInterface $song)
    {
        $this->songs[] = $song;
        $song->setAlbum($this);
    }

    public function removeSong(SongsInterface $song)
    {
        $this->songs->removeElement($song);
        $song->setAlbum($this);
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    /**
     * @return string
     */
    public function getCoverUrl(): string
    {
        return $this->coverUrl;
    }

    /**
     * @param string $coverUrl
     */
    public function setCoverUrl(string $coverUrl): void
    {
        $this->coverUrl = $coverUrl;
    }
}
