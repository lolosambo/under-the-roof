<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models;

use App\Domain\Models\Interfaces\ArticlesInterface;
use App\Domain\Models\Interfaces\GalleriesInterface;
use App\Domain\Models\Interfaces\ImagesInterface;
use App\Domain\Models\Interfaces\VideosInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Galleries
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class Galleries implements GalleriesInterface
{
    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var ArrayCollection
     */
    private $images;

    /**
     * @var ArrayCollection
     */
    private $videos;

    /**
     * @var ArticlesInterface $article
     */
    private $article;

    /**
     * Galleries constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->images = new ArrayCollection();
        $this->videos = new ArrayCollection();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ImagesInterface $image
     */
    public function addImage(ImagesInterface $image)
    {
        $this->images[] = $image;
        $image->setGallery($this);
    }

    /**
     * @param ImagesInterface $image
     */
    public function removeImage(ImagesInterface $image)
    {
        $this->images->removeElement($image);
        $image->setGallery($this);
    }

    /**
     * @return ArrayCollection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * @param VideosInterface $video
     */
    public function addVideo(VideosInterface $video)
    {
        $this->videos[] = $video;
        $video->setGallery($this);
    }

    /**
     * @param VideosInterface $video
     */
    public function removeVideo(VideosInterface $video)
    {
        $this->videos->removeElement($video);
        $video->setGallery($this);
    }

    /**
     * @return ArticlesInterface
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param ArticlesInterface $article
     */
    public function setArticle(ArticlesInterface $article)
    {
        $this->article = $article;
    }
}
