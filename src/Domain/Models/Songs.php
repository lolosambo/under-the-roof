<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models;

use App\Domain\Models\Interfaces\AlbumsInterface;
use App\Domain\Models\Interfaces\SongsInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Songs
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class Songs implements SongsInterface
{
    /**
     * @var UuidInterface $id
     */
    private $id;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var string $author
     */
    private $author = 'Under The Roof';

    /**
     * @var \DateTime
     */
    private $time;

    /**
     * @var string $urlAudio
     */
    private $urlAudio;

    /**
     * @var string $urlVideo
     */
    private $urlVideo;

    /**
     * @var Albums $album
     */
    private $album;

    /**
     * Songs constructor.
     *
     * @param string $title
     */
    public function __construct(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return UuidInterface  $id
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor(string $author)
    {
        $this->author = $author;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param \DateTime $time
     */
    public function setTime(\DateTime $time)
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getUrlAudio()
    {
        return $this->urlAudio;
    }

    /**
     * @param string $urlAudio
     */
    public function setUrlAudio(string $urlAudio)
    {
        $this->urlAudio = $urlAudio;
    }

    /**
     * @return string
     */
    public function getUrlVideo()
    {
        return $this->urlVideo;
    }

    /**
     * @param string $urlAudio
     */
    public function setUrlVideo(string $urlVideo)
    {
        $this->urlVideo = $urlVideo;
    }

    /**
     * @return Albums
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * @param Albums $album
     */
    public function setAlbum(AlbumsInterface $album)
    {
        $this->album = $album;
    }
}


