<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models;

use App\Domain\Models\Interfaces\CommentsInterface;
use Ramsey\Uuid\UuidInterface;
use App\Domain\Models\Interfaces\UsersInterface;
use App\Domain\Models\Interfaces\ArticlesInterface;


/**
 * Class Comments
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class Comments implements CommentsInterface
{
    /**
     * @var UuidInterface $id
     */
    private $id;

    /**
     * @var string $content
     */
    private $content;

    /**
     * @var \DateTime $createdAt
     */
    private $createdAt;

    /**
     * @var UsersInterface $user
     */
    private $user;

    /**
     * @var ArticlesInterface $article
     */
    private $article;

    /**
     * Comments constructor.
     *
     * @param string $content
     */
    public function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime('NOW');
    }

    /**
     * @return UsersInterface
     */
    public function getUser(): UsersInterface
    {
        return $this->user;
    }

    /**
     * @param UsersInterface $user
     */
    public function setUser(UsersInterface $user)
    {
        $this->user = $user;
    }

    /**
     * @return ArticlesInterface
     */
    public function getArticle(): ArticlesInterface
    {
        return $this->article;
    }

    /**
     * @param ArticlesInterface $article
     */
    public function setArticle(ArticlesInterface $article)
    {
        $this->article = $article;
    }

}

