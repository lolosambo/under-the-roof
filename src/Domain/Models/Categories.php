<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models;

use App\Domain\Models\Interfaces\ArticlesInterface;
use App\Domain\Models\Interfaces\CategoriesInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Categories
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class Categories implements CategoriesInterface
{
    /**
     * @var UuidInterface $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var ArrayCollection  $articles
     */
    private $articles;

    /**
     * Categories constructor.
     *
     * @param string $title
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->articles = new ArrayCollection();
    }

    /**
     * @return UuidInterface  $id
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param ArticlesInterface $article
     */
    public function addArticle(ArticlesInterface $article)
    {
        $this->articles[] = $article;
        $article->setCategory($this);
    }

    /**
     * @param ArticlesInterface $article
     */
    public function removeArticle(ArticlesInterface $article)
    {
        $this->articles->removeElement($article);
    }
}



