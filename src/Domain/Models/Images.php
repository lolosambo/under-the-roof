<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models;

use App\Domain\Models\Interfaces\GalleriesInterface;
use App\Domain\Models\Interfaces\ImagesInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class images
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class Images implements ImagesInterface
{
    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var Galleries
     */
    private $gallery;

    /**
     * @var int
     */
    private $aLaUne = 0;

    /**
     * @var string
     */
    private $url;

    /**
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return Galleries
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    public function setGallery(GalleriesInterface $gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * @return int
     */
    public function getALaUne()
    {
        return $this->aLaUne;
    }

    /**
     * @param int $aLaUne
     */
    public function setALaUne(int $aLaUne)
    {
        if (($aLaUne == 0) || ($aLaUne == 1)) {
            $this->aLaUne = $aLaUne;
        }
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }
}
