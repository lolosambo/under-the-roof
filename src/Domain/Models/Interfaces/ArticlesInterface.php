<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models\Interfaces;

use App\Domain\Models\Galleries;
use App\Domain\Models\Interfaces\CategoriesInterface;
use App\Domain\Models\Interfaces\CommentsInterface;
use App\Domain\Models\Interfaces\UsersInterface;
use App\Domain\Models\Interfaces\GalleriesInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;

/**
 * Class ArticlesInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface ArticlesInterface
{
    /**
     * ArticlesInterface constructor.
     *
     * @param string $title
     * @param string $content
     */
    public function __construct(string $title, string $content);

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param $title
     */
    public function setTitle(string $title);

    /**
     * @return string
     */
    public function getSlug();

    /**
     * @param $slug
     */
    public function convertSlug($name);

    /**
     * @return string
     */
    public function getContent(): string;

    /**
     * @param string $content
     *
     * @return mixed
     */
    public function setContent(string $content);

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime;

    /**
     * @return mixed
     */
    public function setCreatedAt();

    /**
     * @return UsersInterface
     */
    public function getUser(): UsersInterface;

    /**
     * @param UsersInterface $user
     */
    public function setUser(UsersInterface $user);

    /**
     * @return ArrayCollection
     */
    public function getComments();

    /**
     * @param CommentsInterface $comment
     */
    public function addComment(CommentsInterface $comment);

    /**
     * @param CommentsInterface $comment
     */
    public function removeComment(CommentsInterface $comment);

    /**
     * @return Galleries
     */
    public function getGallery();

    /**
     * @param GalleriesInterface  $gallery
     */
    public function setGallery(GalleriesInterface $gallery);

    /**
     * @return CategoriesInterface
     */
    public function getCategory();

    /**
     * @param CategoriesInterface $category
     */
    public function setCategory(CategoriesInterface $category);

}

