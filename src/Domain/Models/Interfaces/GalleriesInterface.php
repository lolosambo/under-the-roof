<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models\Interfaces;

use App\Domain\Models\Interfaces\ArticlesInterface;
use App\Domain\Models\Interfaces\ImagesInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;

/**
 * Class GalleriesInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface GalleriesInterface
{
    /**
     * GalleriesInterface constructor.
     *
     * @param string $name
     */
    public function __construct(string $name);

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @return ArrayCollection
     */
    public function getImages();

    /**
     * @param ImagesInterface $image
     */
    public function addImage(ImagesInterface $image);

    /**
     * @param ImagesInterface $image
     */
    public function removeImage(ImagesInterface $image);

    /**
     * @return ArrayCollection
     */
    public function getVideos();

    /**
     * @param VideosInterface $video
     */
    public function addVideo(VideosInterface $video);

    /**
     * @param VideosInterface $video
     */
    public function removeVideo(VideosInterface $video);

    /**
     * @return ArticlesInterface
     */
    public function getArticle();

    /**
     * @param ArticlesInterface $article
     */
    public function setArticle(ArticlesInterface $article);
}