<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models\Interfaces;

use App\Domain\Models\Interfaces\AlbumsInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class SongsInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface SongsInterface
{
    /**
     * SongsInterface constructor.
     *
     * @param string $title
     */
    public function __construct(string $title);

    /**
     * @return UuidInterface  $id
     */
    public function getId(): UuidInterface;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @param string $title
     */
    public function setTitle(string $title);

    /**
     * @return string
     */
    public function getAuthor(): string;

    /**
     * @param string $author
     */
    public function setAuthor(string $author);

    /**
     * @return \DateTime
     */
    public function getTime();

    /**
     * @param \DateTime $time
     */
    public function setTime(\DateTime $time);

    /**
     * @return string
     */
    public function getUrlAudio();

    /**
     * @param string $urlAudio
     */
    public function setUrlAudio(string $urlAudio);

    /**
     * @return string
     */
    public function getUrlVideo();

    /**
     * @param string $urlAudio
     */
    public function setUrlVideo(string $urlVideo);

    /**
     * @return AlbumsInterface
     */
    public function getAlbum();

    /**
     * @param AlbumsInterface $album
     */
    public function setAlbum(AlbumsInterface $album);

}

