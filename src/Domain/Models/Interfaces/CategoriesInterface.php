<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models\Interfaces;

use Ramsey\Uuid\UuidInterface;

/**
 * Class CategoriesInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface CategoriesInterface
{

    /**
     * Categories constructor.
     *
     * @param string $title
     */
    public function __construct(string $name);

    /**
     * @return UuidInterface  $id
     */
    public function getId(): UuidInterface;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     */
    public function setName(string $name);

    /**
     * @param ArticlesInterface $article
     */
    public function addArticle(ArticlesInterface $article);

    /**
     * @param ArticlesInterface $article
     */
    public function removeArticle(ArticlesInterface $article);
}