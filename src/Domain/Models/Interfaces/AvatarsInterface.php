<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models\Interfaces;

use App\Domain\Models\Interfaces\UsersInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AvatarsInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface AvatarsInterface
{
    /**
     * Avatars constructor.
     *
     * @param string $url
     */
    public function __construct(string $url);

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @param string $url
     */
    public function setUrl(string $url): void;

    /**
     * @return UsersInterface
     */
    public function getUser();

    /**
     * @param UsersInterface $user
     */
    public function setUser(UsersInterface $user);
}
