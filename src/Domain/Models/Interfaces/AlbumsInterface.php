<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models\Interfaces;

use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AlbumsInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface AlbumsInterface
{
    /**
     * AlbumsInterface constructor.
     *
     * @param string $title
     * @param int $year
     */
    public function __construct(
        string $title,
        int $year
    );

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @param string $title
     */
    public function setTitle(string $title): void;

    /**
     * @return string
     */
    public function getAuthor(): string;

    /**
     * @param string $author
     */
    public function setAuthor(string $author): void;

    /**
     * @return ArrayCollection
     */
    public function getSongs();

    /**
     * @param SongsInterface $song
     */
    public function addSong(SongsInterface $song);

    /**
     * @param SongsInterface $song
     *
     * @return mixed
     */
    public function removeSong(SongsInterface $song);

    /**
     * @return int
     */
    public function getYear(): int;

    /**
     * @param int $year
     */
    public function setYear(int $year): void;

    /**
     * @return string
     */
    public function getCoverUrl(): string;

    /**
     * @param string $coverUrl
     */
    public function setCoverUrl(string $coverUrl): void;
}
