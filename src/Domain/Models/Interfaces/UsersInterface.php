<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models\Interfaces;

use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;

/**
 * Interface Users
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
interface UsersInterface
{

    /**
     * UsersInterface constructor.
     *
     * @param string  $pseudo
     * @param string  $password
     * @param string  $mail
     */
    public function __construct(
        string $userName,
        string $password,
        string $mail
    );

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @return string
     */
    public function getUsername();

    /**
     * @return null|string
     */
    public function getSalt();

    /**
     * @return string
     */
    public function getMail(): string;

    /**
     * @param $mail
     *
     * @return mixed
     */
    public function setMail($mail);

    /**
     * @return int
     */
    public function getVerified(): int;

    /**
     * @param int $verified
     *
     * @return bool
     */
    public function setVerified(int $verified);

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime;

    public function setCreatedAt();

    /**
     * @return array
     */
    public function getRoles();

    public function eraseCredentials();

    /** @see \Serializable::serialize() */
    public function serialize();

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized);

    /**
     * @return ArrayCollection
     */
    public function getComments();

    /**
     * @param Comments $comment
     */
    public function AddComment($comment);

    /**
     * @param Comments $comment
     */
    public function removeComment($comment);

    /**
     * @return ArrayCollection
     */
    public function getAvatars();

    /**
     * @param Avatars $avatar
     */
    public function AddAvatar($avatar);

    /**
     * @param Avatars $avatar
     */
    public function removeAvatar($avatar);

}


