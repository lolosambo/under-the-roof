<?php


declare(strict_types=1);

/*
 * This file is part of the SnowTricks project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models\Interfaces;
use Ramsey\Uuid\UuidInterface;

/**
 * Interface ImagesInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
interface ImagesInterface
{
    /**
     * @param string $url
     */
    public function __construct(string $url);

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @return mixed
     */
    public function getGallery();

    /**
     * @param GalleriesInterface $trick
     *
     * @return mixed
     */
    public function setGallery(GalleriesInterface $gallery);

    /**
     * @return int
     */
    public function getALaUne();

    /**
     * @param int $aLaUne
     * @return mixed
     */
    public function setALaUne(int $aLaUne);

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @param string $url
     */
    public function setUrl(string $url);

}
