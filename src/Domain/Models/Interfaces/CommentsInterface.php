<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models\Interfaces;

use Ramsey\Uuid\UuidInterface;

/**
 * Class CommentsInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface CommentsInterface
{

    public function __construct(string $content);

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @return string
     */
    public function getContent(): string;

    public function setContent(string $content);

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime;

    /**
     * @return mixed
     */
    public function setCreatedAt();

    /**
     * @return UsersInterface
     */
    public function getUser(): UsersInterface;

    /**
     * @param UsersInterface $user
     */
    public function setUser(UsersInterface $user);


    /**
     * @return ArticlesInterface
     */
    public function getArticle(): ArticlesInterface;

    /**
     * @param ArticlesInterface $article
     */
    public function setArticle(ArticlesInterface $article);
}

