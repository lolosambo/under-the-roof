<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models\Interfaces;

use Ramsey\Uuid\UuidInterface;
use App\Domain\Models\Interfaces\GalleriesInterface;

/**
 * Class videosInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface  VideosInterface
{
    /**
     * @param string $url
     */
    public function __construct(string $url);

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @return GalleriesInterface
     */
    public function getGallery();

    /**
     * @param GalleriesInterface $gallery
     *
     * @return mixed
     */
    public function setGallery(GalleriesInterface $gallery);

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @param string $url
     */
    public function setUrl(string $url): void;
}