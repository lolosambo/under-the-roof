<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models;

use App\Domain\Models\Interfaces\UsersInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;
use App\Domain\Models\Comments;

/**
 * Class Users
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class Users implements UsersInterface
{
    /**
     * @var UuidInterface $id
     */
    private $id;

    /**
     * @var string  $userName
     */
    private $userName;

    /**
     * @var string $password
     */
    private $password;

    /**
     * @var string $mail
     */
    private $mail;

    /**
     * @var int boolean $verified
     */
    private $verified = 0;

    /**
     * @var \DateTime $createdAt
     */
    private $createdAt;

    /**
     * @var ArrayCollection $comments
     */
    private $comments;

    /**
     * @var ArrayCollection  $avatars
     */
    private $avatars;

    /**
     * Users constructor.
     *
     * @param string  $pseudo
     * @param string  $password
     * @param string  $mail
     */
    public function __construct(
        string $userName,
        string $password,
        string $mail
    ) {
        $this->userName = $userName;
        $this->password = $password;
        $this->mail = $mail;
        $this->comments = new ArrayCollection();
        $this->avatars = new ArrayCollection();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->userName;
    }

    /**
     * @return null|string
     */
    public function getSalt()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getMail(): string
    {
        return $this->mail;
    }

    /**
     * @param $mail
     *
     * @return bool
     */
    public function setMail($mail)
    {
        if (preg_match('#^([0-9a-zA-Z-_]+)@([0-9a-zA-Z-_]+).([a-z]+)$#', $mail)) {
            $this->mail = $mail;
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function getVerified(): int
    {
        return $this->verified;
    }

    /**
     * @param int $verified
     *
     * @return bool
     */
    public function setVerified(int $verified)
    {
        if ($verified == 1) {
            $this->verified = $verified;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime('NOW');
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->userName,
            $this->password,
            $this->mail
        ]);
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->userName,
            $this->password,
            $this->mail
            ) = unserialize($serialized);
    }

    /**
     * @return ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param Comments $comment
     */
    public function AddComment($comment)
    {
        $this->comments[] = $comment;
        $comment->setUser($this);
    }

    /**
     * @param Comments $comment
     */
    public function removeComment($comment)
    {
        $this->comments->removeElement($comment);
        $comment->setUser($this);
    }

    /**
     * @return ArrayCollection
     */
    public function getAvatars()
    {
        return $this->avatars;
    }

    /**
     * @param Avatars $avatar
     */
    public function AddAvatar($avatar)
    {
        $this->avatars[] = $avatar;
        $avatar->setUser($this);
    }

    /**
     * @param Avatars $avatar
     */
    public function removeAvatar($avatar)
    {
        $this->avatars->removeElement($avatar);
    }

}

