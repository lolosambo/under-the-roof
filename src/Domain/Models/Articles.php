<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models;

use App\Domain\Models\Interfaces\ArticlesInterface;
use App\Domain\Models\Interfaces\CategoriesInterface;
use App\Domain\Models\Interfaces\CommentsInterface;
use App\Domain\Models\Interfaces\GalleriesInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\UuidInterface;
use App\Domain\Models\Interfaces\UsersInterface;


/**
 * Class Comments
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class Articles implements ArticlesInterface
{
    /**
     * @var UuidInterface $id
     */
    private $id;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var string $slug
     */
    private $slug;

    /**
     * @var string $content
     */
    private $content;

    /**
     * @var \DateTime $createdAt
     */
    private $createdAt;

    /**
     * @var UsersInterface $user
     */
    private $user;

    /**
     * @var ArrayCollection
     */
    private $comments;

    /**
     * @var Galleries $gallery
     */
    private $gallery;

    /**
     * @var CategoriesInterface $category
     */
    private $category;

    /**
     * Articles constructor.
     *
     * @param string $title
     * @param string $content
     */
    public function __construct(string $title, string $content)
    {
        $this->title = $title;
        $this->content = $content;
        $this->comments = new ArrayCollection();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param $slug
     */
    public function convertSlug($name)
    {
        $this->slug = str_replace(' ', '_', $name);
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime('NOW');
    }

    /**
     * @return UsersInterface
     */
    public function getUser(): UsersInterface
    {
        return $this->user;
    }

    /**
     * @param UsersInterface $user
     */
    public function setUser(UsersInterface $user)
    {
        $this->user = $user;
    }

    /**
 * @return ArrayCollection
 */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param CommentsInterface $comment
     */
    public function addComment(CommentsInterface $comment)
    {
        $this->comments[] = $comment;
        $comment->setArticle($this);
    }

    /**
     * @param CommentsInterface $comment
     */
    public function removeComment(CommentsInterface $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * @return Galleries
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param Galleries $gallery
     */
    public function setGallery(GalleriesInterface $gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * @return CategoriesInterface
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param CategoriesInterface $category
     */
    public function setCategory(CategoriesInterface $category)
    {
        $this->category = $category;
    }
}
