<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Models;

use App\Domain\Models\Interfaces\GalleriesInterface;
use App\Domain\Models\Interfaces\VideosInterface;
use Ramsey\Uuid\UuidInterface;

/**
 * Class videos
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class Videos implements VideosInterface
{
    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var Galleries
     */
    private $gallery;

    /**
     * @var string
     */
    private $url;

    /**
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return Galleries
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param GalleriesInterface $gallery
     *
     * @return mixed|void
     */
    public function setGallery(GalleriesInterface $gallery)
    {
        $this->gallery = $gallery;
    }


    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }
}
