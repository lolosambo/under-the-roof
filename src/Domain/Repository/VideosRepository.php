<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Repository;

use App\Domain\Models\Videos;
use App\Domain\Models\Interfaces\VideosInterface;
use App\Domain\Repository\Interfaces\VideosRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class VideosRepository.
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class VideosRepository extends ServiceEntityRepository implements VideosRepositoryInterface
{
    /**
     * VideosRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Videos::class);
    }

    /**
     * @param UuidInterface $videoId
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findVideo(UuidInterface $videoId)
    {
        return $this->createQueryBuilder('v')
            ->where('v.id = ?1')
            ->setParameter(1, $videoId)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return mixed
     */
    public function findAllVideos()
    {
        return $this->createQueryBuilder('v')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param VideosInterface $videoId
     *
     * @return mixed
     */
    public function deleteVideo(VideosInterface $videoId)
    {
        return $this->createQueryBuilder('v')
            ->delete()
            ->where('v.id = ?1')
            ->setParameter(1, $videoId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param $video
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($video)
    {
        $this->getEntityManager()->persist($video);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }
}
