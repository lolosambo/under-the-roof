<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Repository;

use App\Domain\Models\Articles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ArticlesRepository.
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class ArticlesRepository extends ServiceEntityRepository implements ArticlesRepositoryInterface
{
    /**
     * UsersRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Articles::class);
    }

    /**
     * @param UuidInterface $articleId
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findArticle(UuidInterface $articleId)
    {
        return $this->createQueryBuilder('a')
            ->where('a.id = ?1')
            ->setParameter(1, $articleId)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param UuidInterface $articleId
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findArticleBySlug(string $slug)
    {
        return $this->createQueryBuilder('a')
            ->where('a.slug = ?1')
            ->setParameter(1, $slug)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $pseudo
     * @param $password
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByPseudoAndPassword($pseudo, $password)
    {
        return $this->createQueryBuilder('u')
            ->where('u.pseudo = :pseudo')
            ->setParameter('pseudo', $pseudo)
            ->andWhere('u.password = :password')
            ->setParameter('password', sha1($password))
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $pseudo
     * @param $mail
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByPseudoAndMail($pseudo, $mail)
    {
        return $this->createQueryBuilder('u')
            ->where('u.pseudo = :pseudo')
            ->setParameter('pseudo', $pseudo)
            ->andWhere('u.mail = :mail')
            ->setParameter('mail', $mail)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $pseudo
     * @param $mail
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByMail($mail)
    {
        return $this->createQueryBuilder('u')
            ->Where('u.mail = :mail')
            ->setParameter('mail', $mail)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $user
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($user)
    {
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }
}
