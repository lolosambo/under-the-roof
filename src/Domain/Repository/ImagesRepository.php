<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Repository;

use App\Domain\Models\Images;
use App\Domain\Models\Interfaces\ImagesInterface;
use App\Domain\Repository\Interfaces\ImagesRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ImagesRepository.
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class ImagesRepository extends ServiceEntityRepository implements ImagesRepositoryInterface
{
    /**
     * ImagesRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Images::class);
    }

    /**
     * @param UuidInterface $imageId
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findImage(UuidInterface $imageId)
    {
        return $this->createQueryBuilder('i')
            ->where('i.id = ?1')
            ->setParameter(1, $imageId)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return mixed
     */
    public function findAllImages()
    {
        return $this->createQueryBuilder('i')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ImagesInterface $imageId
     *
     * @return mixed
     */
    public function deleteImage(ImagesInterface $imageId)
    {
        return $this->createQueryBuilder('i')
            ->delete()
            ->where('i.id = ?1')
            ->setParameter(1, $imageId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param ImagesInterface $image
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(ImagesInterface $image)
    {
        $this->getEntityManager()->persist($image);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }
}
