<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Repository;

use App\Domain\Models\Songs;
use App\Domain\Models\Interfaces\SongsInterface;
use App\Domain\Repository\Interfaces\SongsRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class SongsRepository.
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class SongsRepository extends ServiceEntityRepository implements SongsRepositoryInterface
{
    /**
     * SongsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Songs::class);
    }

    /**
     * @param UuidInterface $songId
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findSong(UuidInterface $songId)
    {
        return $this->createQueryBuilder('s')
            ->where('s.id = ?1')
            ->setParameter(1, $songId)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return mixed
     */
    public function findAllSongs()
    {
        return $this->createQueryBuilder('s')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param SongsInterface $songId
     *
     * @return mixed
     */
    public function deleteSong(SongsInterface $songId)
    {
        return $this->createQueryBuilder('s')
            ->delete()
            ->where('s.id = ?1')
            ->setParameter(1, $songId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param $song
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($song)
    {
        $this->getEntityManager()->persist($song);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }
}
