<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Repository;

use App\Domain\Models\Interfaces\CommentsInterface;
use App\Domain\Repository\Interfaces\CommentsRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class CommentsRepository.
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CommentsRepository extends ServiceEntityRepository implements CommentsRepositoryInterface
{
    /**
     * CommentsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Comments::class);
    }

    /**
     * @param UuidInterface $userId
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findComment(UuidInterface $commentId)
    {
        return $this->createQueryBuilder('c')
            ->where('c.id = ?1')
            ->setParameter(1, $commentId)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param UuidInterface $articleId
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findAllComments(UUIDInterface $articleId)
    {
        return $this->createQueryBuilder('c')
            ->leftJoin('c.article', 'ca', "WITH", "ca.id = ?1")
            ->setParameter(1, $articleId)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param CommentsInterface $commentId
     * @return mixed
     */
    public function deleteComment(CommentsInterface $commentId)
    {
        return $this->createQueryBuilder('c')
            ->delete()
            ->where('c.id = ?1')
            ->setParameter(1, $commentId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param $comment
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(CommentsInterface $comment)
    {
        $this->getEntityManager()->persist($comment);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }
}
