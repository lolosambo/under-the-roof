<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Repository;

use App\Domain\Models\Albums;
use App\Domain\Models\Interfaces\AlbumsInterface;
use App\Domain\Repository\Interfaces\AlbumsRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class AlbumsRepository.
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class AlbumsRepository extends ServiceEntityRepository implements AlbumsRepositoryInterface
{
    /**
     * AlbumsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Albums::class);
    }

    /**
     * @param UuidInterface $userId
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findAlbum(UuidInterface $albumId)
    {
        return $this->createQueryBuilder('a')
            ->where('a.id = ?1')
            ->setParameter(1, $albumId)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return mixed
     */
    public function findAllAlbums()
    {
        return $this->createQueryBuilder('c')
            ->setCacheable(true)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param AlbumsInterface $userId
     * @return mixed
     */
    public function deleteArticle(AlbumsInterface $albumId)
    {
        return $this->createQueryBuilder('a')
            ->delete()
            ->where('a.id = ?1')
            ->setParameter(1, $albumId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param $album
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(AlbumsInterface $album)
    {
        $this->getEntityManager()->persist($album);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }
}
