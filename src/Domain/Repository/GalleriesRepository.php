<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Repository;

use App\Domain\Models\Galleries;
use App\Domain\Models\Interfaces\GalleriesInterface;
use App\Domain\Repository\Interfaces\GalleriesRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class GalleriesRepository.
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class GalleriesRepository extends ServiceEntityRepository implements GalleriesRepositoryInterface
{
    /**
     * GalleriesRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Galleries::class);
    }

    /**
     * @param UuidInterface $galleryId
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findGallery(UuidInterface $galleryId)
    {
        return $this->createQueryBuilder('g')
            ->where('g.id = ?1')
            ->setParameter(1, $galleryId)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return mixed
     */
    public function findAllGalleries()
    {
        return $this->createQueryBuilder('g')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param GalleriesInterface $galleryId
     *
     * @return mixed
     */
    public function deleteGallery(GalleriesInterface $galleryId)
    {
        return $this->createQueryBuilder('g')
            ->delete()
            ->where('g.id = ?1')
            ->setParameter(1, $galleryId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param $gallery
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($gallery)
    {
        $this->getEntityManager()->persist($gallery);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }
}
