<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Repository;

use App\Domain\Models\Avatars;
use App\Domain\Models\Interfaces\AvatarsInterface;
use App\Domain\Repository\Interfaces\AvatarsRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class AvatarsRepository.
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class AvatarsRepository extends ServiceEntityRepository implements AvatarsRepositoryInterface
{
    /**
     * AvatarsRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Avatars::class);
    }

    /**
     * @param UuidInterface $avatarId
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findAvatar(UuidInterface $avatarId)
    {
        return $this->createQueryBuilder('a')
            ->where('a.id = ?1')
            ->setParameter(1, $avatarId)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return mixed
     */
    public function findAllAvatars()
    {
        return $this->createQueryBuilder('a')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param AvatarsInterface $avatarId
     *
     * @return mixed
     */
    public function deleteAvatar(AvatarsInterface $avatarId)
    {
        return $this->createQueryBuilder('a')
            ->delete()
            ->where('a.id = ?1')
            ->setParameter(1, $avatarId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param $avatar
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($avatar)
    {
        $this->getEntityManager()->persist($avatar);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }
}
