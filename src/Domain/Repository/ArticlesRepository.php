<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Repository;

use App\Domain\Models\Articles;
use App\Domain\Models\Interfaces\ArticlesInterface;
use App\Domain\Repository\Interfaces\ArticlesRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ArticlesRepository.
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class ArticlesRepository extends ServiceEntityRepository implements ArticlesRepositoryInterface
{
    /**
     * UsersRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Articles::class);
    }

    /**
     * @param UuidInterface $articleId
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findArticle(UuidInterface $articleId)
    {
        return $this->createQueryBuilder('a')
            ->where('a.id = ?1')
            ->setParameter(1, $articleId)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $slug
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findArticleBySlug(string $slug)
    {
        return $this->createQueryBuilder('a')
            ->where('a.slug = ?1')
            ->setParameter(1, $slug)
            ->setCacheable(true)
            ->getQuery()
            ->getOneOrNullResult();
    }


    /**
     * @return mixed
     */
    public function findAllArticles()
    {
        return $this->createQueryBuilder('a')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ArticlesInterface $userId
     * @return mixed
     */
    public function deleteArticle(ArticlesInterface $articleId)
    {
        return $this->createQueryBuilder('a')
            ->delete()
            ->where('a.id = ?1')
            ->setParameter(1, $articleId)
            ->getQuery()
            ->execute();
    }

    /**
     * @param ArticlesInterface $article
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(ArticlesInterface $article)
    {
        $this->getEntityManager()->persist($article);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }
}
