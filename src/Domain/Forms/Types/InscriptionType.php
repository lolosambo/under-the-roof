<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Forms\Types;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;


/**
 * Class InscriptionType
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class InscriptionType extends Form
{
    /**
     * @var FormFactoryInterface $form
     */
    private $factory;

    /**
     * InscriptionType constructor.
     * @param FormFactoryInterface $form
     */
    public function __construct(FormFactoryInterface $form, $options)
    {
        $this->factory = $form;
    }


}
