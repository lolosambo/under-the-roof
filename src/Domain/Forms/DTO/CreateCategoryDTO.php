<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Forms\DTO;

use App\Domain\Forms\DTO\Interfaces\CreateCategoryDTOInterface;

/**
 * Class CreateCategoryDTO
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CreateCategoryDTO implements CreateCategoryDTOInterface
{

    /**
     * @var string $title
     */
    public $name;

    /**
     * CreateCategoryDTO constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }
}