<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Forms\DTO;

use App\Domain\Forms\DTO\Interfaces\CreateAlbumDTOInterface;

/**
 * Class CreateAlbumDTO
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CreateAlbumDTO implements CreateAlbumDTOInterface
{

    /**
     * @var string $title
     */
    public $title;

    /**
     * @var string $author
     */
    public $author;

    /**
     * @var int $year
     */
    public $year;

    /**
     * @var string $coverUrl
     */
    public $coverUrl;

    /**
     * CreateAlbumDTO constructor.
     *
     * @param string $title
     * @param string $author
     * @param int $year
     * @param string $coverurl
     */
    public function __construct(
        string $title,
        string $author,
        int $year,
        string $coverUrl
) {
        $this->title = $title;
        $this->author = $author;
        $this->year = $year;
        $this->coverUrl = $coverUrl;
    }
}
