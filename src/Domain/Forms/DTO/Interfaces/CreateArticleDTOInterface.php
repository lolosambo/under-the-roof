<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace App\Domain\Forms\DTO\Interfaces;

use App\Domain\Models\Interfaces\CategoriesInterface;

/**
 * Class CreateArticleDTOInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface CreateArticleDTOInterface
{
    /**
     * CreateArticleDTO constructor.
     * @param string $title
     * @param string $content
     * @param CategoriesInterface $category
     */
    public function __construct(
        string $title,
        string $content,
        CategoriesInterface $category
    );
}