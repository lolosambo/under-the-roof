<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Forms\DTO\Interfaces;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class CreateGalleryDTOInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
interface CreateGalleryDTOInterface
{
    /**
     * CreateGalleryDTOInterface constructor.
     *
     * @param string $title
     * @param string $author
     * @param int $year
     * @param string $coverurl
     */
    public function __construct(
        string $name,
        ArrayCollection $images,
        ArrayCollection $videos
    );
}