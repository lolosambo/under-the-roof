<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace App\Domain\Forms\DTO\Interfaces;


/**
 * Interface CreateUserDTOInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface CreateUserDTOInterface
{

    /**
     * CreateUserDTOInterface constructor.
     *
     * @param string  $pseudo
     * @param string  $password
     * @param string  $mail
     */
    public function __construct(
        string $username,
        string $password,
        string $mail
    );

}
