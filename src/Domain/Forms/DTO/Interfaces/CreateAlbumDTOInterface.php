<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Domain\Forms\DTO\Interfaces;

/**
 * Class CreateAlbumDTOInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface CreateAlbumDTOInterface
{
    /**
     * CreateAlbumDTOInterface constructor.
     *
     * @param string $title
     * @param string $author
     * @param int $year
     * @param string $coverurl
     */
    public function __construct(
        string $title,
        string $author,
        int $year,
        string $coverurl
    );
}