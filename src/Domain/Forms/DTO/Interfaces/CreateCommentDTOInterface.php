<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace App\Domain\Forms\DTO\Interfaces;

/**
 * Class CreateCommentDTOInterface
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
Interface CreateCommentDTOInterface
{

    /**
     * CreateCommentDTOInterface constructor.
     *
     * @param string $content
     */
    public function __construct(string $content);
}