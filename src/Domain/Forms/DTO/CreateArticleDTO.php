<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace App\Domain\Forms\DTO;

use App\Domain\Forms\DTO\Interfaces\CreateArticleDTOInterface;
use App\Domain\Models\Interfaces\CategoriesInterface;

/**
 * Class CreateArticleDTO
 *
 * @author Laurent BERTON <lolosambo2@gmail.com>
 */
class CreateArticleDTO implements CreateArticleDTOInterface
{
    /**
     * @var CategoriesInterface
     */
    public $category;

    /**
     * @var string  $title
     */
    public $title;

    /**
     * @var string $mail
     */
    public $content;

    /**
     * CreateArticleDTO constructor.
     * @param string $title
     * @param string $content
     * @param CategoriesInterface $category
     */
    public function __construct(
        string $title,
        string $content,
        CategoriesInterface $category
    ) {
        $this->title = $title;
        $this->content = $content;
        $this->category = $category;
    }
}
