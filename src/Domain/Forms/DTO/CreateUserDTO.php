<?php

declare(strict_types=1);

/*
 * This file is part of the Under The Roof project.
 *
 * (c) Laurent BERTON <lolosambo2@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace App\Domain\Forms\DTO;

use App\Domain\Forms\DTO\Interfaces\CreateUserDTOInterface;

/**
* Class CreateUserDTO
*
* @author Laurent BERTON <lolosambo2@gmail.com>
*/
class CreateUserDTO implements CreateUserDTOInterface
{

/**
* @var string  $username
*/
public $username;

/**
* @var string $password
*/
public $password;

/**
* @var string $mail
*/
public $mail;


/**
* CreateUserDTO constructor.
*
* @param string  $pseudo
* @param string  $password
* @param string  $mail
*/
public function __construct(
    string $username,
    string $password,
    string $mail
) {
    $this->username = $username;
    $this->password = $password;
    $this->mail = $mail;
}

}

