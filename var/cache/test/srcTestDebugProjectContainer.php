<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerC0d7WiI\srcTestDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerC0d7WiI/srcTestDebugProjectContainer.php') {
    touch(__DIR__.'/ContainerC0d7WiI.legacy');

    return;
}

if (!\class_exists(srcTestDebugProjectContainer::class, false)) {
    \class_alias(\ContainerC0d7WiI\srcTestDebugProjectContainer::class, srcTestDebugProjectContainer::class, false);
}

return new \ContainerC0d7WiI\srcTestDebugProjectContainer(array(
    'container.build_hash' => 'C0d7WiI',
    'container.build_id' => 'd87d517f',
    'container.build_time' => 1532099111,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerC0d7WiI');
